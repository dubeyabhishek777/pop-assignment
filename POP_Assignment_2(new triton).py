import sys
from triton import *

# to store symbolic variable constraints
symbolicVar_Constraints = []

# possible length of each argv string
strlen = 100
   
Triton = TritonContext()
ast = Triton.getAstContext()

# convert symbolic variable constraints from list to tree
def Convert_Constraints(constraints_list):
    AllConstraints = ast.equal(ast.bvtrue(), ast.bvtrue())
    for index in range(len(constraints_list)):
        AllConstraints = ast.land([AllConstraints, constraints_list[index]])
    return AllConstraints


# print constraint argvs
def print_constraints(seed):
    global strlen

    for j in range(10):
        string = []
        #print 'STR %d : '%(j+1),
        for i in range(0,strlen):
            address = (2100+(j*100))+i
            if address in seed:
                string.append(chr(seed[2100+j*100+i]%256))
        print ''.join(string)


# emulating instruction
def emulate(pc,DEST,seed):
    #print '[+] Emulation Begun'
    while pc:
        
        # If DEST addr reached
        if DEST == pc:
            #print 'FINAL Seed injected:', seed
            print_constraints(seed)
            sys.exit(0) 
        
        # Fetch opcodes
        opcodes = Triton.getConcreteMemoryAreaValue(pc, 16)

        # Create the Triton instruction
        instruction = Instruction()
        instruction.setOpcode(opcodes)
        instruction.setAddress(pc)

        # Process
        Triton.processing(instruction)
        #print instruction

        # Next
        pc = Triton.getConcreteRegisterValue(Triton.registers.eip)
    
    return


# Accumulate Branches not taken in current paths
def getNewPaths():   
    global symbolicVar_Constraints
    global branchesTaken

    # Set of new inputs
    new_paths=[]

    # starting with some seed input
    previous_Constraints = ast.equal(ast.bvtrue(), ast.bvtrue())
    final_constraints = ast.equal(ast.bvtrue(), ast.bvtrue())

    # getting constraint on symbolic variable
    final_constraints = Convert_Constraints(symbolicVar_Constraints)

    # path constraints extracted from previous execution
    path_Constraints = Triton.getPathConstraints()

    # loop over all path constraints   
    for pc in path_Constraints:
        # If condition have multiple branches
        if pc.isMultipleBranches():
            branches = pc.getBranchConstraints()
            # Iterate over all branches
            for branch in branches:
                #collect constraint for Not taken(false) branches
                if branch['isTaken'] == False:
                    # add symbolic Variable constraints to path constraints and ask for model                    
                    models = Triton.getModel(ast.land([final_constraints,previous_Constraints, branch['constraint']]))
                    seed   = dict()

                    # Update seed from model
                    for k, v in models.items():                     
                        symbolic_Var = Triton.getSymbolicVariableFromId(k)
                        seed.update({symbolic_Var.getKindValue(): v.getValue()})

                    if seed:
                        new_paths.append(seed)
		        #print "NEW INPUT",seed

        # Add True branch constraints to previous constraints
        previous_Constraints = ast.land([previous_Constraints, pc.getTakenPathConstraintAst()])

    # Clear the path constraints for next execution
    Triton.clearPathConstraints()
    return new_paths


def loadBinary(path):
    import lief
    # Map the binary into the memory
    binary = lief.parse(path)
    phdrs  = binary.segments
    for phdr in phdrs:
        size   = phdr.physical_size
        vaddr  = phdr.virtual_address
        #print('Loading 0x%06x - 0x%06x' %(vaddr, vaddr+size))
        Triton.setConcreteMemoryAreaValue(vaddr, phdr.content)
    return
   

# Symbolise argc, argv string locations, setting Stack Pointer, Base Pointer
def symbolizeInputs(seed):
    # Clean symbolic state
    global strlen
    global symbolicVar_Constraints

    string_array = 1000
    string_ptr = 2000

    Triton.concretizeAllRegister()
    Triton.concretizeAllMemory()

    #EBP must be 4 less than ESP, to compansate for "PUSH EBP"
    Triton.setConcreteRegisterValue(Triton.registers.ebp, 0x7fffffff-4)
    Triton.setConcreteRegisterValue(Triton.registers.esp, 0x7fffffff)
    
    #symbolise ARGC
    string_byte = Triton.convertMemoryToSymbolicVariable(MemoryAccess(Triton.getConcreteRegisterValue(Triton.registers.ebp)+8, CPUSIZE.WORD))

    #concetrize ARGV reference
    Triton.setConcreteMemoryValue(MemoryAccess(Triton.getConcreteRegisterValue(Triton.registers.ebp)+12, CPUSIZE.WORD), string_array)
       
    for address, value in seed.items():
        if address == Triton.getConcreteRegisterValue(Triton.registers.ebp)+8:
            Triton.setConcreteMemoryValue(MemoryAccess(address, CPUSIZE.WORD), value)
            Triton.convertMemoryToSymbolicVariable(MemoryAccess(address, CPUSIZE.WORD))
        else:
            Triton.setConcreteMemoryValue(address, value)
            string_byte = Triton.convertMemoryToSymbolicVariable(MemoryAccess(address, CPUSIZE.BYTE))
            #applying constraints over symbolic string characters
            symbolicVar_Constraints.append(ast.bvule(ast.variable(string_byte), ast.bv(0x7E, 8)))
            symbolicVar_Constraints.append(ast.bvuge(ast.variable(string_byte), ast.bv(0x21, 8)))
 
    return


# symbolise argv locations
def initContext():

    string_array = 1000
    string_ptr = 2000

    for i in range(1,11):
        Triton.setConcreteMemoryValue(MemoryAccess(string_array+i*4, CPUSIZE.WORD), string_ptr + i*100)   
        for j in range(strlen):
            string_byte=Triton.convertMemoryToSymbolicVariable(MemoryAccess(string_ptr + i*100 +j, CPUSIZE.BYTE))
            #applying constraints over symbolic string characters
            symbolicVar_Constraints.append(ast.bvule(ast.variable(string_byte), ast.bv(0x7E, 8)))
            symbolicVar_Constraints.append(ast.bvuge(ast.variable(string_byte), ast.bv(0x21, 8)))
    
    return


# main
if __name__ == '__main__':

    target_file = sys.argv[1]
    ENTRY = int(sys.argv[2],16)
    DEST = int(sys.argv[3],16)

    # Define the target architecture
    Triton.setArchitecture(ARCH.X86)
    
    # Load the binary
    loadBinary(target_file)   

    #symbolise argv memory
    initContext()

    # Start with random seed at 2100
    lastInput = list()
    worklist  = list([{2100:21}])

    while worklist:
        
        # Take first seed from list
        seed = worklist[0]

        # Symbolize inputs(argvs)
        symbolizeInputs(seed)

        # Emulate
        emulate(ENTRY,DEST,seed)
        
        lastInput += [dict(seed)]
        del worklist[0]

        newInputs = getNewPaths()
        #print newInputs
        for inputs in newInputs:
            if inputs not in lastInput and inputs not in worklist:
                worklist += [dict(inputs)]
        del symbolicVar_Constraints[:]
    sys.exit(0)
